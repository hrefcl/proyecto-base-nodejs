module.exports = function(app, sync) {

    var sequelize = app.sequelize;
    var dataTypes = app.Sequelize;
    var bcrypt = require('bcrypt');

    var User = sequelize.define('User', {
        status: dataTypes.ENUM('active', 'locked', 'delete', 'suspended', 'unverified', 'inactive'),
        dni: dataTypes.STRING(255),
        firstname: dataTypes.STRING(255),
        lastname: dataTypes.STRING(255),
        email: {
            type: dataTypes.STRING(255),
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        password: dataTypes.STRING(255),
        birthday: dataTypes.STRING(255),
        gender: dataTypes.STRING(255),
        token: dataTypes.STRING(255),
        role: dataTypes.ENUM('admin', 'company', 'user'),
        profile_pic: {
            type: dataTypes.STRING(255),
            defaultValue: 'default_snuuper.png'
        },
        setup: dataTypes.TEXT,
        facebook_id: dataTypes.STRING(255),
    }, {
        tableName: 'user'
    });

    function hashPassword(user, callback) {
        bcrypt.genSalt(10, function(err, salt) {
            if (err) {
                return callback(err);
            } else {
                bcrypt.hash(user.password, salt, function(err, hash) {
                    if (err) {
                        return callback(err);
                    } else {
                        user.password = hash;
                        return callback(null, user);
                    }
                });
            }
        });
    }

    if (!app.utils) app.utils = {};
    app.hashPassword = hashPassword;

    User.beforeCreate(function(user, fn) {
        user.token = Math.random().toString(36).substr(2);

        function validateEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }
        if (user && user.email) {
            user.email = user.email.trim();
            if (!validateEmail(user.email)) {
                return fn('Correo Incorrecto', null)
            }
        }
        hashPassword(user, function(err, user) {
            if (err) {
                return fn(err, null);
            } else {
                return fn(null, user);
            }
        });
    });


    User.afterCreate(function(user, fn) {
        console.log('despues de crear el usuario')
    });

    var Country = sequelize.define('Country', {
        name: dataTypes.STRING(45),
        prefix: dataTypes.STRING(45)
    }, {
        tableName: 'country'
    });

    var Region = sequelize.define('Region', {
        name: dataTypes.STRING(45)
    }, {
        tableName: 'region'
    });

    var Province = sequelize.define('Province', {
        name: dataTypes.STRING(45)
    }, {
        tableName: 'province'
    });

    var Commune = sequelize.define('Commune', {
        name: dataTypes.STRING(45)
    }, {
        tableName: 'commune'
    });


    var Profiling = sequelize.define('Profiling', {
        'profile': {
            type: dataTypes.BOOLEAN,
            defaultValue: true
        },
        'dashboard': {
            type: dataTypes.BOOLEAN,
            defaultValue: false
        },
        'notifications': {
            type: dataTypes.BOOLEAN,
            defaultValue: false
        },
        'messages': {
            type: dataTypes.BOOLEAN,
            defaultValue: false
        },
        'results': {
            type: dataTypes.BOOLEAN,
            defaultValue: false
        },
        'edit_profile': {
            type: dataTypes.BOOLEAN,
            defaultValue: false
        },
        'view_profile': {
            type: dataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        tableName: 'profiling'
    });

    // Relationships for Profiling
    Profiling.belongsTo(User, {
        foreignKey: 'user_id',
        as: 'profiling'
    });

    // Relationships for User
    User.hasOne(Profiling, {
        foreignKey: 'user_id',
        as: 'profiling'
    });
    User.belongsTo(Country, {
        foreignKey: 'country_id',
        as: 'country'
    });
    User.belongsTo(Region, {
        foreignKey: 'region_id',
        as: 'region'
    });
    User.belongsTo(Province, {
        foreignKey: 'province_id',
        as: 'province'
    });
    User.belongsTo(Commune, {
        foreignKey: 'commune_id',
        as: 'commune'
    });

    // Country
    Country.hasMany(Region, {
        foreignKey: 'country_id',
        as: 'regions'
    });
    Country.hasMany(User, {
        foreignKey: 'country_id',
        as: 'users'
    });

    // Region
    Region.belongsTo(Country, {
        foreignKey: 'country_id',
        as: 'country'
    });
    Region.hasMany(Province, {
        foreignKey: 'region_id',
        as: 'provinces'
    });
    Region.hasMany(User, {
        foreignKey: 'region_id',
        as: 'users'
    });

    // Province
    Province.belongsTo(Region, {
        foreignKey: 'region_id',
        as: 'region'
    });
    Province.hasMany(Commune, {
        foreignKey: 'province_id',
        as: 'communes'
    });
    Province.hasMany(User, {
        foreignKey: 'province_id',
        as: 'users'
    });
    // Commune
    Commune.belongsTo(Province, {
        foreignKey: 'province_id',
        as: 'province'
    });
    Commune.hasMany(User, {
        foreignKey: 'commune_id',
        as: 'users'
    });


    app.schemas = {
        User: User,
        Country: Country,
        Region: Region,
        Province: Province,
        Commune: Commune,
        Profiling: Profiling
    };
};
