var express = require('express');
var http = require("http");
var app = express();
app.path = require('path');
app.prettyjson = require('prettyjson');
app.static = require('node-static');
app.vhost = require('vhost');
app.async = require('async');
app.cors = require('cors');
app.os = require("os");
app.httpProxy = require('http-proxy');
app.Sequelize = require("sequelize");
app.fs = require('fs');
require('./config')(app,express);
//console.log(process.env);
var port = 8080;
if (process.env.HOSTNAME == "cyzom") {
  port = 80;
}

http.createServer(app).listen(port);