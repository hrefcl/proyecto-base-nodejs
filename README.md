Proyecto Base en NodeJS cargado con recursos basicos

### Para que es este repositorio?? ###

* Resumen Rapido
* Version 1.1


### ¿Cómo puedo configurar? ###

* Resumen de configurar
* Como configura el servidor.
Actualmente esta funcionando en un CENTOS 7

>##IMPORTANTE EL HOST DEL SERVIDOR DEBE LLAMARSE XXXX y se debe cambiar dicho valor en el archivo app.js y config.js 

>###Ejecutar Siguintes condigo en servidor desde 0
>     
>     sudo yum -y update
>     sudo yum -y install epel-release
>     sudo yum -y install nodejs
>     sudo yum -y install npm
>     sudo yum -y install scl-utils
>     sudo yum -y groupinstall "Development Tools"
>     sudo yum -y install gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel
>     sudo yum -y install wget
>     sudo yum -y install GraphicsMagick-devel
>     sudo yum -y install zlib-devel
>     sudo yum -y install bzip2-devel
>     sudo yum -y install zlib-devel
>     sudo yum -y install bzip2-devel
>     sudo yum -y install ncurses-devel
>     sudo yum -y install gcc-c++ openssl-devel
>     cd /opt
>     wget --no-check-certificate https://www.python.org/ftp/python/2.7.6/Python-2.7.6.tar.xz
>     tar xf Python-2.7.6.tar.xz
>     cd Python-2.7.6
>     ./configure --prefix=/usr/local
>     make && make altinstall
>     wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.tar.gz"
>     tar xzf jdk-7u79-linux-x64.tar.gz
>     cd ..
>     java -version
>     cd jdk1.7.0_79/
>     alternatives --install /usr/bin/java java /opt/jdk1.7.0_79/bin/java 2
>     alternatives --config java
>     alternatives --install /usr/bin/jar jar /opt/jdk1.7.0_79/bin/jar 2
>     alternatives --install /usr/bin/javac javac /opt/jdk1.7.0_79/bin/javac 2
>     alternatives --set jar /opt/jdk1.7.0_79/bin/jar
>     alternatives --set javac /opt/jdk1.7.0_79/bin/javac
>     java -version
>     export JAVA_HOME=/opt/jdk1.7.0_79
>     export JRE_HOME=/opt/jdk1.7.0_79/jre
>     export PATH=$PATH:/opt/jdk1.7.0_79/bin:/opt/jdk1.7.0_79/jre/bin
>     yum install ruby
>     ruby -v
>     yum install ant
>     ant -version
>     npm install bcrypt -g
>     sudo npm install pm2@latest -g
>     sudo npm install nodemon -g
>     

>##Configurar accesos SSH en bitbucket
>     
>     ssh-keygen
>     cat ~/.ssh/id_rsa.pub
>     
> Copiar clave rsa a llevero de bitbucket


>##Configuraciones para correr la aplicacion
>Bajar aplicacion desdes bitbucket 
> 
>     cd ~  
>     git config --global user.email "git@nombreproyecto.cl"
>     git config --global user.name "Server Nombre Proyecto"
>     git clone git@bitbucket.org:#####/#####.git
>     cd nombreproyecto
>     npm install
>     pm2 start processes.json
>     

* dependencias
* Configuración de base de datos

Tabla Usuarios


            CREATE TABLE `User` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `status` enum('active','locked','delete','suspended','unverified','inactive') DEFAULT 'active',
          `dni` varchar(255) DEFAULT NULL,
          `firstname` varchar(255) DEFAULT NULL,
          `lastname` varchar(255) DEFAULT NULL,
          `email` varchar(255) DEFAULT NULL,
          `password` varchar(255) DEFAULT NULL,
          `birthday` varchar(255) DEFAULT NULL,
          `gender` varchar(255) DEFAULT NULL,
          `token` varchar(255) DEFAULT NULL,
          `role` enum('admin','company','user') DEFAULT NULL,
          `setup` text,
          `facebook_id` varchar(255) DEFAULT NULL,
          `createdAt` datetime NOT NULL,
          `updatedAt` datetime NOT NULL,
          `country_id` int(11) DEFAULT NULL,
          `region_id` int(11) DEFAULT NULL,
          `province_id` int(11) DEFAULT NULL,
          `commune_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `email_UNIQUE` (`email`),
          KEY `country_id` (`country_id`),
          KEY `region_id` (`region_id`),
          KEY `province_id` (`province_id`),
          KEY `commune_id` (`commune_id`),
          CONSTRAINT `User_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
          CONSTRAINT `User_ibfk_2` FOREIGN KEY (`region_id`) REFERENCES `Region` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
          CONSTRAINT `User_ibfk_4` FOREIGN KEY (`province_id`) REFERENCES `Province` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
          CONSTRAINT `User_ibfk_5` FOREIGN KEY (`commune_id`) REFERENCES `Commune` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


Tabla Perfiles

        CREATE TABLE `Profiling` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `user_id` int(11) NOT NULL DEFAULT '0',
          `profile` tinyint(1) DEFAULT '1',
          `dashboard` tinyint(1) DEFAULT '0',
          `notifications` tinyint(1) DEFAULT '0',
          `messages` tinyint(1) DEFAULT '0',
          `results` tinyint(1) DEFAULT '0',
          `edit_profile` tinyint(1) DEFAULT '0',
          `view_profile` tinyint(1) DEFAULT '0',
          `createdAt` datetime NOT NULL,
          `updatedAt` datetime NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `Profiling_ibfk_1` (`user_id`),
          CONSTRAINT `Profiling_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

Tabla Paises

        CREATE TABLE `Country` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(45) DEFAULT NULL,
          `prefix` varchar(45) DEFAULT NULL,
          `createdAt` datetime NOT NULL,
          `updatedAt` datetime NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

Tabla Regiones

        CREATE TABLE `Region` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(45) DEFAULT NULL,
          `createdAt` datetime NOT NULL,
          `updatedAt` datetime NOT NULL,
          `country_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `country_id` (`country_id`),
          CONSTRAINT `Region_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

Tabla Provincias

        CREATE TABLE `Province` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(45) DEFAULT NULL,
          `createdAt` datetime NOT NULL,
          `updatedAt` datetime NOT NULL,
          `region_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `region_id` (`region_id`),
          CONSTRAINT `Province_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `Region` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


Tabla Comunas

        CREATE TABLE `Commune` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(45) DEFAULT NULL,
          `createdAt` datetime NOT NULL,
          `updatedAt` datetime NOT NULL,
          `province_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `province_id` (`province_id`),
          CONSTRAINT `Commune_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `Province` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



* Cómo ejecutar pruebas
* Instrucciones de implementación

### Directrices Contribución ###

* Pruebas de Escritura
* Revisión de código
* Otras directrices

### ¿A quién debo hablar? ###

* Francisco.arenas@href.cl
* info@href.cl