module.exports = function(app, express) {
    var logging = false;
    app.sequelize = new app.Sequelize('NombreBaseDeDatos', 'root', 'Password', {
        host: '127.0.0.1',
        port: 3306,
        logging: logging,
    });

    if (app.os.hostname() == "NombreDelHost") {
        console.log("Starting proxy");
        // para crear servicios independintes con url indendientes
        /*
        app.proxy = app.httpProxy.createProxyServer({});
        var newAdminExtStatic = new app.static.Server(app.path.resolve(__dirname, '../nodejs/admin/'));
        
        //Proxy a una servidor de servicios
        app.use(app.vhost('clientesapi.urldelProyeto.cl', function(req, res) {
            app.proxy.web(req, res, {
                target: 'http://127.0.0.1:3090'
            });
        }));
        //Proxy a un pagina statica
        app.use(app.vhost('admin.urldelProyeto.cl', function(req, res) {
            req.addListener('end', function() {
                newAdminExtStatic.serve(req, res);
            }).resume();
        }));

        */
    }

    // url del archivo html por defecto en www.urldelProyeto.cl en este caso el index.html de portos
    app.use(express.static(app.path.join(__dirname, '/')));
};
